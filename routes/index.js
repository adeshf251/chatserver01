var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var min=100000;
  var max=999999;
  var random =Math.floor(Math.random() * (+max - +min)) + +min;

  var username = 'user_name'+random;
  res.render('index', { title: 'Express', username: username });
});

router.get('/p1', function(req, res, next) {
  res.render('index2', { title: 'Express' });
});

router.get('/p2', function(req, res, next) {
  res.render('index3', { title: 'Express' });
});


module.exports = router;
