var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var messageController = require("./controllers/messageController");

// // const Server = require('socket.io');
// // const io = new Server();

// const io = require('socket.io')(12501, {
//   path: '/test',
//   serveClient: false,
//   // below are engine.IO options
//   pingInterval: 10000,
//   pingTimeout: 5000,
//   cookie: false
// });

var indexRouter = require("./routes/index");
// var usersRouter = require('./routes/users');

// var app = express();

// // view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'hbs');

// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// module.exports = app;





var app = require("express")();
var server = require("http").Server(app);
var io = require("socket.io")(server);

var mongoose = require("mongoose");
var configDB = require("./config/database.config");
mongoose.Promise = global.Promise;
if (mongoose.connect(configDB.url)) {
  console.log("connected to db");
} // connect to our database

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

server.listen(12001);
// WARNING: app.listen(80) will NOT work here!

var arrayx = [];
for (let index = 0; index < 5000; index++) {
  var message = {
    from: 'from_name_here',
    date_time: "mkdlkvskdvsd sdsdnklvsdkv sdvsdklvsdksdklvklsdvnsdv sdvkdskdsk",
    other1: "mkdlkvskdvsd sdsdnklvsdkv sdvsdklvsdksdklvklsdvnsdv sdvkdskdsk",
    other2: "mkdlkvskdvsd sdsdnklvsdkv sdvsdklvsdksdklvklsdvnsdv sdvkdskdsk",
    other3: "mkdlkvskdvsd sdsdnklvsdkv sdvsdklvsdksdklvklsdvnsdv sdvkdskdsk",
    other4: "mkdlkvskdvsd sdsdnklvsdkv sdvsdklvsdksdklvklsdvnsdv sdvkdskdsk",
    other5: "mkdlkvskdvsd sdsdnklvsdkv sdvsdklvsdksdklvklsdvnsdv sdvkdskdsk",
  }
  arrayx.push(message);
}

app.use("/", indexRouter);

var nsp = io.of("/my-namespace");
nsp.on("connection", function(socket) {
  console.log("someone connected");

  socket.emit("hi", "everyone!");

  socket.on("send", function(data) {
    var message = {
      msg: data.data,
      from: data.username,
      date_time: Date.now(),
      msg_type: "text",
    }

    /* save to database */
    messageController.insertNewMessage('5cd1d816e4588fa5d955c849', data.data, "text", data.username);
    /* now broadcast it to avaliable sockets */
    socket.broadcast.emit('send', message);    
  });

  socket.on("retrive", function(lastMsgID){
    /* 
    retrive 100 the newer data from the database and send it over socket 
    lastMsgID => null/undefined ... means client don't have any reference
    */
  });

  socket.on("retriveall", function(lastMsgID){
    /* 
    retrive all the newer data from the database and send it over socket 
    lastMsgID => null/undefined ... means client don't have any reference
    
    To Do : set the max upper limit
    */
   var data = messageController.retriveMessages();
   data.then((res) => {
     console.log("sended ..." );
     socket.emit('send', res);   
   })
  });

  socket.on("loadprevious", function(msgID) {
    /* load 50 messages previous to the given id */
  });

  socket.on("loadnew", function(msgID) {
    /* load next 50 messages to the given ID */
  });

  socket.on("msgAvaliable", function(msgID){
    /* matches the recieved id with the last_id saved in database ...
     if not equal than return true --> new message avaliable
     else retrun false --> NO new message avaliable
    */
  });

  socket.on("totalAvaliable", function(msgID) {
    /* return the count of the new message avaliable than the given ID */
  });

});




nsp.emit("hi", "everyone!");

nsp.on("send", function(data) {
  console.log(data);
});
nsp.on("typing", function(data) {
  console.log(`user is typing, $(data) `);
});

// io.on('connection', function (socket) {
//   socket.emit('news', { hello: 'world' });
//   socket.on('my other event', function (data) {
//     console.log(data);
//   });
// });
