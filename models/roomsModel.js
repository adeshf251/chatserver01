var mongoose = require('mongoose');

// Define schema
var Schema = mongoose.Schema;

var myRooms = new Schema({
    room_name:  { type: String, default: null },
    admin_id:  { type: String, default: null },
    members_id: [],
    type: { type: String, default: 'direct' },
    status: { type: String, default: 'active' },
    deleted_at: { type: String, default: null },
    updated_at: { type: Date, default: Date.now }
});

/*
*   --------- type --------
*   0 - direct (one-to-one)
*   1 - group (multiple students)
*
*/


// Compile model from schema
module.exports = mongoose.model('rooms', myRooms);