var mongoose = require('mongoose');

// Define schema
var Schema = mongoose.Schema;

var myUser = new Schema({
    username:  { type: String, default: null },
    name:  { type: String, default: null },
    email:  { type: String, default: null },
    type: { type: String, default: 'student' },
    status: { type: String, default: 'active' },
    deleted_at: { type: String, default: null },
    updated_at: { type: Date, default: Date.now }
});

/*
*   --------- type --------
*   0 - student
*   1 - individual_educator
*   2 - admission_guide
*   3 - college_admin
*   4 - coaching_institutes_admin
*   5 - information_advertiser
*
*/


// Compile model from schema
module.exports = mongoose.model('users', myUser);