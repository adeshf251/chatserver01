var mongoose = require('mongoose');

var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;;


var messageObject = new Schema({
    room_id:  { type: ObjectId },
    msg:  { type: String, default: null },
    msg_type:  { type: String, default: null }, 
    from:  { type: String, default: null }, 
    status: { type: Number, default: null }, 
    voilation: { type: Number, default: null },
    deleted_at: { type: String, default: null },
    updated_at: { type: Date, default: Date.now }
});


/*
*   -------- status ----------
*   0 - Not monitored (for group)
*   1 - not recieved
*   2 - recieved but not read
*   3 - recieved and read
*   5 - waiting for ADMIN review
*
*
*   -------- voilation --------
*   0 - NO voilation
*   1 - contact sharing voilation
*   5 - marked as sespecious
*   6 - spam
*   7 - pornograpy spam
*/



// Compile model from schema
module.exports = mongoose.model('messages', messageObject);