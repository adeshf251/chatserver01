var roomsModel = require('../models/roomsModel');
var messagesModel = require('../models/messagesModel');
var usersModel = require('../models/usersModel');


module.exports.insertNewMessage = (room_id, msg, msg_type, from) => {
    /* inset into messages */
    messagesModel.create(
        {
            room_id: room_id,
            msg: msg,
            msg_type: msg_type,
            from: from,
            status: 0,
            voilation: 0,
          done: true,
          auto: true
        },
        function (err, rowData) {
            if (err) {
                console.log(err);
                return {status: "error"};
            }
            if (!rowData) { 
                console.log("failed");
                return {status: "failed"};
            }
            console.log("success");
            return {status: "success"};
        });
}
module.exports.retriveAllMessages = () => {
    /* inset into messages */
    return new Promise((resolve, reject) => {
        messagesModel.find(
            { deleted_at: null },
            { _id:1, msg:1, msg_type:1, from:1 },
            function (err, rowData) {
                if (err) { console.log('error occured '+ err);  return reject([]);  }
            })
            .then(function (result) {
                resolve(result);
            })
            .catch(function (err) {
                console.log("error in promise ... catch handled ");
                reject([]);
            });

    });
}
module.exports.retriveMessages = () => {
    /* inset into messages */

    var sort;
    if(sort===undefined) { sort=-1; }

    var limit;
    if(limit===undefined) { limit=100; }

    var start;
    // var start = req.query.start;
    if(start===undefined) { start=0; }

    return new Promise((resolve, reject) => {
        messagesModel.find(
            { deleted_at: null },
            { _id:1, msg:1, msg_type:1, from:1 },
            { sort: {'_id': sort }, skip: Number(start), limit: Number(limit) },
            function (err, rowData) {
                if (err) { console.log('error occured '+ err);  return reject([]);  }
            })
            .then(function (result) {
                resolve(result);
            })
            .catch(function (err) {
                console.log("error in promise ... catch handled ");
                reject([]);
            });

    });

}
module.exports.reloadSingleMessage = () => {
    /* inset into messages */
}
module.exports.reloadMultipleMessages = () => {
    /* inset into messages */
}
module.exports.markMessageStatus = () => {
    /* inset into messages */
}
module.exports.markMessageVoilation = () => {
    /* inset into messages */
}

